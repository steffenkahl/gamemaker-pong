{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "04b_03",
  "styleName": "Regular",
  "size": 20.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 0,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":9,"h":19,"character":32,"shift":9,"offset":0,},
    "33": {"x":181,"y":44,"w":2,"h":19,"character":33,"shift":5,"offset":0,},
    "34": {"x":172,"y":44,"w":7,"h":19,"character":34,"shift":9,"offset":0,},
    "35": {"x":158,"y":44,"w":12,"h":19,"character":35,"shift":14,"offset":0,},
    "36": {"x":147,"y":44,"w":9,"h":19,"character":36,"shift":12,"offset":0,},
    "37": {"x":133,"y":44,"w":12,"h":19,"character":37,"shift":14,"offset":0,},
    "38": {"x":122,"y":44,"w":9,"h":19,"character":38,"shift":12,"offset":0,},
    "39": {"x":118,"y":44,"w":2,"h":19,"character":39,"shift":5,"offset":0,},
    "40": {"x":111,"y":44,"w":5,"h":19,"character":40,"shift":7,"offset":0,},
    "41": {"x":104,"y":44,"w":5,"h":19,"character":41,"shift":7,"offset":0,},
    "42": {"x":185,"y":44,"w":7,"h":19,"character":42,"shift":9,"offset":0,},
    "43": {"x":95,"y":44,"w":7,"h":19,"character":43,"shift":9,"offset":0,},
    "44": {"x":77,"y":44,"w":5,"h":19,"character":44,"shift":7,"offset":0,},
    "45": {"x":68,"y":44,"w":7,"h":19,"character":45,"shift":9,"offset":0,},
    "46": {"x":64,"y":44,"w":2,"h":19,"character":46,"shift":5,"offset":0,},
    "47": {"x":53,"y":44,"w":9,"h":19,"character":47,"shift":12,"offset":0,},
    "48": {"x":42,"y":44,"w":9,"h":19,"character":48,"shift":12,"offset":0,},
    "49": {"x":35,"y":44,"w":5,"h":19,"character":49,"shift":7,"offset":0,},
    "50": {"x":24,"y":44,"w":9,"h":19,"character":50,"shift":12,"offset":0,},
    "51": {"x":13,"y":44,"w":9,"h":19,"character":51,"shift":12,"offset":0,},
    "52": {"x":2,"y":44,"w":9,"h":19,"character":52,"shift":12,"offset":0,},
    "53": {"x":84,"y":44,"w":9,"h":19,"character":53,"shift":12,"offset":0,},
    "54": {"x":205,"y":44,"w":9,"h":19,"character":54,"shift":12,"offset":0,},
    "55": {"x":73,"y":65,"w":9,"h":19,"character":55,"shift":12,"offset":0,},
    "56": {"x":216,"y":44,"w":9,"h":19,"character":56,"shift":12,"offset":0,},
    "57": {"x":164,"y":65,"w":9,"h":19,"character":57,"shift":12,"offset":0,},
    "58": {"x":160,"y":65,"w":2,"h":19,"character":58,"shift":5,"offset":0,},
    "59": {"x":156,"y":65,"w":2,"h":19,"character":59,"shift":5,"offset":0,},
    "60": {"x":147,"y":65,"w":7,"h":19,"character":60,"shift":9,"offset":0,},
    "61": {"x":138,"y":65,"w":7,"h":19,"character":61,"shift":9,"offset":0,},
    "62": {"x":129,"y":65,"w":7,"h":19,"character":62,"shift":9,"offset":0,},
    "63": {"x":118,"y":65,"w":9,"h":19,"character":63,"shift":12,"offset":0,},
    "64": {"x":104,"y":65,"w":12,"h":19,"character":64,"shift":14,"offset":0,},
    "65": {"x":93,"y":65,"w":9,"h":19,"character":65,"shift":12,"offset":0,},
    "66": {"x":175,"y":65,"w":9,"h":19,"character":66,"shift":12,"offset":0,},
    "67": {"x":84,"y":65,"w":7,"h":19,"character":67,"shift":9,"offset":0,},
    "68": {"x":62,"y":65,"w":9,"h":19,"character":68,"shift":12,"offset":0,},
    "69": {"x":53,"y":65,"w":7,"h":19,"character":69,"shift":9,"offset":0,},
    "70": {"x":44,"y":65,"w":7,"h":19,"character":70,"shift":9,"offset":0,},
    "71": {"x":33,"y":65,"w":9,"h":19,"character":71,"shift":12,"offset":0,},
    "72": {"x":22,"y":65,"w":9,"h":19,"character":72,"shift":12,"offset":0,},
    "73": {"x":13,"y":65,"w":7,"h":19,"character":73,"shift":9,"offset":0,},
    "74": {"x":2,"y":65,"w":9,"h":19,"character":74,"shift":12,"offset":0,},
    "75": {"x":236,"y":44,"w":9,"h":19,"character":75,"shift":12,"offset":0,},
    "76": {"x":227,"y":44,"w":7,"h":19,"character":76,"shift":9,"offset":0,},
    "77": {"x":233,"y":23,"w":12,"h":19,"character":77,"shift":14,"offset":0,},
    "78": {"x":194,"y":44,"w":9,"h":19,"character":78,"shift":12,"offset":0,},
    "79": {"x":222,"y":23,"w":9,"h":19,"character":79,"shift":12,"offset":0,},
    "80": {"x":227,"y":2,"w":9,"h":19,"character":80,"shift":12,"offset":0,},
    "81": {"x":207,"y":2,"w":9,"h":19,"character":81,"shift":12,"offset":0,},
    "82": {"x":196,"y":2,"w":9,"h":19,"character":82,"shift":12,"offset":0,},
    "83": {"x":185,"y":2,"w":9,"h":19,"character":83,"shift":12,"offset":0,},
    "84": {"x":176,"y":2,"w":7,"h":19,"character":84,"shift":9,"offset":0,},
    "85": {"x":165,"y":2,"w":9,"h":19,"character":85,"shift":12,"offset":0,},
    "86": {"x":154,"y":2,"w":9,"h":19,"character":86,"shift":12,"offset":0,},
    "87": {"x":140,"y":2,"w":12,"h":19,"character":87,"shift":14,"offset":0,},
    "88": {"x":129,"y":2,"w":9,"h":19,"character":88,"shift":12,"offset":0,},
    "89": {"x":118,"y":2,"w":9,"h":19,"character":89,"shift":12,"offset":0,},
    "90": {"x":218,"y":2,"w":7,"h":19,"character":90,"shift":9,"offset":0,},
    "91": {"x":111,"y":2,"w":5,"h":19,"character":91,"shift":7,"offset":0,},
    "92": {"x":89,"y":2,"w":9,"h":19,"character":92,"shift":12,"offset":0,},
    "93": {"x":82,"y":2,"w":5,"h":19,"character":93,"shift":7,"offset":0,},
    "94": {"x":73,"y":2,"w":7,"h":19,"character":94,"shift":9,"offset":0,},
    "95": {"x":62,"y":2,"w":9,"h":19,"character":95,"shift":12,"offset":0,},
    "96": {"x":55,"y":2,"w":5,"h":19,"character":96,"shift":7,"offset":0,},
    "97": {"x":44,"y":2,"w":9,"h":19,"character":97,"shift":12,"offset":0,},
    "98": {"x":33,"y":2,"w":9,"h":19,"character":98,"shift":12,"offset":0,},
    "99": {"x":24,"y":2,"w":7,"h":19,"character":99,"shift":9,"offset":0,},
    "100": {"x":13,"y":2,"w":9,"h":19,"character":100,"shift":12,"offset":0,},
    "101": {"x":100,"y":2,"w":9,"h":19,"character":101,"shift":12,"offset":0,},
    "102": {"x":238,"y":2,"w":7,"h":19,"character":102,"shift":9,"offset":0,},
    "103": {"x":100,"y":23,"w":9,"h":19,"character":103,"shift":12,"offset":0,},
    "104": {"x":2,"y":23,"w":9,"h":19,"character":104,"shift":12,"offset":0,},
    "105": {"x":200,"y":23,"w":2,"h":19,"character":105,"shift":5,"offset":0,},
    "106": {"x":193,"y":23,"w":5,"h":19,"character":106,"shift":7,"offset":0,},
    "107": {"x":184,"y":23,"w":7,"h":19,"character":107,"shift":9,"offset":0,},
    "108": {"x":180,"y":23,"w":2,"h":19,"character":108,"shift":5,"offset":0,},
    "109": {"x":166,"y":23,"w":12,"h":19,"character":109,"shift":14,"offset":0,},
    "110": {"x":155,"y":23,"w":9,"h":19,"character":110,"shift":12,"offset":0,},
    "111": {"x":144,"y":23,"w":9,"h":19,"character":111,"shift":12,"offset":0,},
    "112": {"x":133,"y":23,"w":9,"h":19,"character":112,"shift":12,"offset":0,},
    "113": {"x":122,"y":23,"w":9,"h":19,"character":113,"shift":12,"offset":0,},
    "114": {"x":204,"y":23,"w":7,"h":19,"character":114,"shift":9,"offset":0,},
    "115": {"x":111,"y":23,"w":9,"h":19,"character":115,"shift":12,"offset":0,},
    "116": {"x":93,"y":23,"w":5,"h":19,"character":116,"shift":7,"offset":0,},
    "117": {"x":82,"y":23,"w":9,"h":19,"character":117,"shift":12,"offset":0,},
    "118": {"x":71,"y":23,"w":9,"h":19,"character":118,"shift":12,"offset":0,},
    "119": {"x":57,"y":23,"w":12,"h":19,"character":119,"shift":14,"offset":0,},
    "120": {"x":48,"y":23,"w":7,"h":19,"character":120,"shift":9,"offset":0,},
    "121": {"x":37,"y":23,"w":9,"h":19,"character":121,"shift":12,"offset":0,},
    "122": {"x":26,"y":23,"w":9,"h":19,"character":122,"shift":12,"offset":0,},
    "123": {"x":17,"y":23,"w":7,"h":19,"character":123,"shift":9,"offset":0,},
    "124": {"x":13,"y":23,"w":2,"h":19,"character":124,"shift":5,"offset":0,},
    "125": {"x":213,"y":23,"w":7,"h":19,"character":125,"shift":9,"offset":0,},
    "126": {"x":186,"y":65,"w":9,"h":19,"character":126,"shift":12,"offset":0,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":127,},
  ],
  "regenerateBitmap": true,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "font_headline",
  "tags": [],
  "resourceType": "GMFont",
}