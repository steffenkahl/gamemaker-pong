draw_set_font(fnt_headline);
draw_set_halign(fa_center);
draw_set_alpha(1);
draw_text(room_width / 2,200,"PONG PARTY");


draw_set_font(fnt_menuoption);
draw_set_valign(fa_center);

function DrawOption(location, text){
	if(location == selectedMenuOption) {
		draw_set_alpha(1);
	} else {
		draw_set_alpha(0.3);
	}
	
	draw_text(room_width / 2,200 + (100*location), text);
}

//DRAW MENU
DrawOption(1,"START PONG");
DrawOption(2,"START PONGBREAKOUT");
DrawOption(3,"EXIT GAME");

