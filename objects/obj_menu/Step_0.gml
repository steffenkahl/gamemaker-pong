//MENU 
forceddirection = 0;

if(keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_up)) {
	forceddirection -= 1;
}

if(keyboard_check_pressed(ord("S")) || keyboard_check_pressed(vk_down)) {
	forceddirection += 1;
}

if(forceddirection > 0){
	selectedMenuOption += 1;
}

if(forceddirection < 0){
	selectedMenuOption -= 1;
}

if(selectedMenuOption <= 0) 
	selectedMenuOption = optionsCount;
	
if(selectedMenuOption > optionsCount)
	selectedMenuOption = 1;
	
//SELECT OPTION
if(keyboard_check_pressed(vk_enter) || keyboard_check_pressed(vk_space)) {
	draw_set_alpha(1);
	if(selectedMenuOption == 1){
		room_goto(rm_pong);
	}
	if(selectedMenuOption == 2){
		room_goto(rm_pongbreakout);
	}
	if(selectedMenuOption == 3){
		game_end();
	}
}

